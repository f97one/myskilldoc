============
スキルマスタ
============

概要
====

スキルのマスタデータを格納する。

物理名
======

skill_mst

カラム定義
==========

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - skill_id
     - 一貫番号
     - serial
     - 1
     - True
     - 
     - 
     - スキルの一貫番号
   * - skill_name
     - スキル名
     - varchar(64)
     - 
     - True
     - ''
     - 
     - スキルの表示名
   * - category_id
     - カテゴリーID
     - int
     - 
     - False
     - 
     - 
     - スキルが属するカテゴリーID
   * - seq_no
     - 表示シーケンス
     - int
     - 
     - True
     - 0
     - 
     - 画面上の並び順
     
外部キー制約
============

.. list-table::
   :header-rows: 1

   * - キー物理名
     - 参照元カラム
     - 参照元カーディナリティ
     - 参照先カラム
     - 参照先カーディナリティ
     - 更新時オプション
     - 削除時オプション
   * - skill_mst_category_id_fk
     - skill_mst.category_id
     - 0 または 1
     - skill_category.category_id
     - 0以上
     - （なし）
     - on delete set null


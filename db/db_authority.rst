========
操作権限
========

概要
====

ユーザーの操作権限を定める。

物理名
======

authority

カラム定義
==========

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - authority_id
     - 組織ID
     - character varying(16)
     - 1
     - True
     - 
     - 
     - 
   * - authority_name
     - 組織名
     - varchar(32)
     - 
     - False
     - ''
     - 
     - 
   * - enabled
     - 有効フラグ
     - boolean
     - 
     - False
     - true
     - 
     - 
   * - created_timestamp
     - 作成日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 
   * - updated_timestamp
     - レコード更新日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 

外部キー制約
============

なし

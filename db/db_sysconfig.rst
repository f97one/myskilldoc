============
システム設定
============

概要
====

システムで使用する定数等の管理を行う。

物理名
======

system_config

カラム定義
==========

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - config_key
     - キー
     - character varying(128)
     - 1
     - True
     - ''
     - 
     - 設定値のキー
   * - config_val
     - 値
     - character varying(256)
     - 
     - True
     - ''
     - 
     - キーに対応する設定値

外部キー制約
============

なし
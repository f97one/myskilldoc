============
スキルレベル
============

概要
====

スキルのレベルパラメータ管理を行う。

物理名
======

skill_level

カラム定義
==========

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - level_id
     - レベル番号
     - int
     - 1
     - True
     - 
     - 
     - レベルの一環番号
   * - level_val
     - レベル
     - varchar(2)
     - 
     - True
     - ''
     - 
     - レベルの画面表示値
   * - seq_no
     - 表示シーケンス
     - int
     - 
     - True
     - 0
     - 
     - 画面上の並び順
   * - created_timestamp
     - 作成日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 
   * - updated_timestamp
     - レコード更新日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 

外部キー制約
============

なし

==============
ユーザースキル
==============

概要
====

ユーザー保有のスキルの管理を行う。

物理名
======

user_skill

カラム定義
==========

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - login_id
     - ログインID
     - character varying(50)
     - 
     - True
     - 
     - 
     - スキル保有者のアカウント名
   * - skill_id
     - スキルID
     - integer
     - 
     - True
     - 
     - 
     - 保有スキルのID
   * - level_id
     - レベルID
     - integer
     - 
     - True
     - 
     - 
     - 保有スキルのレベルID
   * - created_timestamp
     - 作成日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 
   * - updated_timestamp
     - レコード更新日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 

外部キー制約
============

.. list-table::
   :header-rows: 1

   * - キー物理名
     - 参照元カラム
     - 参照元カーディナリティ
     - 参照先カラム
     - 参照先カーディナリティ
     - 更新時オプション
     - 削除時オプション
   * - userskill_username_fk
     - userskill.username
     - 0 または 1
     - users.username
     - 0以上
     - 
     - on delete cascade
   * - userskill_skill_id_fk
     - userskill.skill_id
     - 0 または 1
     - skill_mst.skill_id
     - 0以上
     - 
     - on delete set to null
   * - userskill_level_id_fk
     - userskill.skill_level
     - 0 または 1
     - skill_level.level_id
     - 0以上
     - 
     - on delete set to null

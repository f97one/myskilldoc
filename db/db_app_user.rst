========
ユーザー
========

概要
====

ユーザーアカウントの管理を行う。

物理名
======

app_user

カラム定義
==========

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - login_id
     - ログインID
     - character varying(50)
     - 1
     - 
     - 
     - unique
     - ユーザのアカウント名（＝ログインID）
   * - password
     - パスワード
     - character varying(128)
     - 
     - True
     - 
     - 
     - 
   * - display_name
     - 表示名
     - character varying(128)
     - 
     - False
     - 
     - 
     - 画面表示に使う名前
   * - authority_id
     - 権限ID
     - character varying(16)
     - 
     - True
     - 
     - 
     - 
   * - organization_id
     - 所属する組織階層ID
     - int
     - 
     - True
     - 
     - 
     - 
   * - organization_path
     - 組織階層
     - character varying(255)
     - 
     - False
     - 
     - 
     - 組織を/で区切ったリテラルを格納
   * - mail_address
     - メールアドレス
     - character varying(255)
     - 
     - True
     - 
     - 
     - 
   * - enabled
     - 有効フラグ
     - boolean
     - 
     - False
     - true
     - 
     - ログイン可能か否かを示す
   * - created_timestamp
     - 作成日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 
   * - updated_timestamp
     - レコード更新日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - これをもって最終ログイン日時を表す

外部キー制約
============

.. list-table::
   :header-rows: 1

   * - キー物理名
     - 参照元カラム
     - 参照元カーディナリティ
     - 参照先カラム
     - 参照先カーディナリティ
     - 更新時オプション
     - 削除時オプション
   * - users_organization_id_fk
     - users.organization_id
     - 0 または 1
     - organization.organization_id
     - 0以上
     - （なし）
     - on delete set null


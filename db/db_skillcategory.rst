==================
スキルカテゴリ定義
==================

概要
====

スキルの分類を格納する。

物理名
======

skill_category

カラム定義
==========

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - skill_category_id
     - カテゴリ番号
     - serial
     - 1
     - True
     - 
     - 
     - 
   * - parent_category_id
     - 親カテゴリ番号
     - integer
     - 
     - False
     - 
     - 
     - 
   * - skill_category_name
     - カテゴリー名
     - character varying (16)
     - 
     - False
     - ''
     - 
     - カテゴリーの表示名
   * - skill_category_path
     - スキルカテゴリ階層
     - character varying (255)
     - 
     - False
     - 
     - 
     - 
   * - enabled
     - 有効フラグ
     - boolean
     - 
     - False
     - true
     - 
     - 
   * - created_timestamp
     - 作成日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 
   * - updated_timestamp
     - レコード更新日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 

外部キー制約
============

なし

========
組織階層
========

概要
====

組織の階層定義を格納する。

物理名
======

organization

カラム定義
==========

.. list-table::
   :header-rows: 1

   * - 物理名
     - 論理名
     - 型
     - 主キー順序
     - NOT NULL制約
     - デフォルト
     - DDLオプション
     - コメント
   * - organization_id
     - 組織ID
     - int
     - 1
     - True
     - 
     - 
     - 組織階層の一貫番号
   * - org_name
     - 組織名
     - character varying(64)
     - 
     - True
     - ''
     - 
     - 組織階層の表示名
   * - parent_id
     - 上位組織ID
     - integer
     - 
     - False
     - 
     - 
     - その組織階層の上位にあたる組織階層ID
   * - enabled
     - 有効フラグ
     - boolean
     - 
     - False
     - true
     - 
     - 
   * - created_timestamp
     - 作成日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 
   * - updated_timestamp
     - レコード更新日時
     - timestamp without time zone
     - 
     - False
     - now()
     - 
     - 

外部キー制約
============

なし

============
画面遷移設計
============

.. blockdiag::

   blockdiag screen_transition {
     orientation = portrait

     login [label = "ログイン"];
     my_menu [label = "マイメニュー"];
     profile_reg [label = "プロファイル登録"];
     profile_reg_edit [label = "プロファイル登録 入力"];
     profile_reg_confirm [label = "プロファイル登録 入力確認"];
     profile_reg_comp [label = "プロファイル登録 完了"];
     skill_reg [label = "スキル登録"];
     skill_reg_edit [label = "スキル登録 入力"];
     skill_reg_confirm [label = "スキル入力 入力確認"];
     skill_reg_comp [label = "スキル入力 完了"];
     skill_find [label = "スキル検索"];

     login -> my_menu -> profile_reg -> profile_reg_edit -> profile_reg_confirm -> profile_reg_comp -> my_menu
              my_menu -> skill_reg -> skill_reg_edit -> skill_reg_confirm -> skill_reg_comp -> my_menu
              my_menu -> skill_find -> my_menu

   }

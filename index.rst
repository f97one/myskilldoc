.. MySkill documentation master file, created by
   sphinx-quickstart on Sat Jan 20 22:37:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================
MySkill 設計文書
================


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   requirements
   screen_transition
   db/index
   screen/index


索引と目次
==========

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

==========
マイページ
==========


画面イメージ
============

.. image:: ../_static/mock_mypage.png

画面部品
========

.. list-table::
   :header-rows: 1
   
   * - 論理名
     - 種類
     - I/O
     - 最大桁数
     - 入力規則
     - 初期値
     - 導入元
     - 備考
   * - エラー表示
     - ラベル
     - O
     - 
     - 
     - 
     - 
     - 
   * - 保有スキル表
     - テーブル
     - O
     - 
     - 
     - 
     - 
     - 
   * - スキルカテゴリ
     - ラベル
     - O
     - 
     - 
     - カテゴリー
     - userskill.skill_id, skillmst.category_id, skillcategory.category_name
     - 
   * - 保有スキル表
     - ラベル
     - O
     - 
     - 
     - スキル名
     - userskill.skill_id, skillmst.skill_name
     - 
   * - レベル表示
     - ラベル
     - O
     - 
     - 
     - レベル
     - userskill.skill_level, skill_level.level_val
     - 

イベント処理
============

呼び出し時
----------

.. csv-table::
   :widths: 3, 10

   URL ,{コンテキスト}/mypage
   メソッド, GET


1. セッションからログインユーザーの `users.user_id` を取得する。
2. ユーザー保有スキルのコレクションを取得する。

   .. csv-table::
      :widths: 5, 15
      
      取得項目, スキルカテゴリ定義.スキルカテゴリ名、スキル.スキル名、レベル.スキルレベル
      対象テーブル, ユーザー保有スキル、スキル、レベル、スキルカテゴリ定義
      抽出条件, ユーザー保有スキル.ユーザーID = `users.user_id`
      ソート順, ユーザー保有スキル.スキルID（昇順）
   
3. ユーザー保有スキルのコレクションを保有スキル表にセットする。

#!/bin/bash
HTML_ROOTDIR=/var/www/html/designdoc/myskill
UID_TOMCAT=tomcat
UID_WWW=www-data

LANG_CD=ja

HTML_DIST=$HTML_ROOTDIR/$LANG_CD
EPUB_DIST=$HTML_ROOTDIR/epub/$LANG_CD
PDF_DIST=$HTML_ROOTDIR/pdf/$LANG_CD

make clean html epub latexpdf

PS4='+ [${BASH_SOURCE}:${LINENO}] ${FUNCNAME:+$FUNCNAME(): }'
set -vx

if [ ! -e $HTML_ROOTDIR ]; then
    mkdir $HTML_ROOTDIR
fi

sudo chown -R $UID_TOMCAT:$UID_TOMCAT $HTML_ROOTDIR

if [ ! -e $HTML_DIST ]; then
    mkdir -p $HTML_DIST
fi
rsync -rlDH --delete _build/html $HTML_DIST

if [ ! -e $EPUB_DIST ]; then
    mkdir -p $EPUB_DIST
fi
cp -f _build/epub/MySkill.epub $EPUB_DIST

if [ ! -e $PDF_DIST ]; then
    mkdir -p $PDF_DIST
fi
cp -f _build/latex/MySkill.pdf $PDF_DIST

sudo chown -R $UID_WWW $HTML_ROOTDIR

set +vx
